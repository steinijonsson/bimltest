use Steini

-- select * from Postalcodes
go

alter function dbo.fnPostal() --( @pnr varchar ( 3 ) )
returns @ret table ( Area varchar ( 50 ), Location varchar ( 50 ) )
as
  begin
    insert @ret ( Area, Location )
    select Area, [Location] from Postalcodes --where Postcode = @pnr

    return
  end
go

alter view vwPostal
as
  select Area, [Location] from Postalcodes
go


-- select * from fnPostal ( '201' )
select * from dbo.fnPostal()
